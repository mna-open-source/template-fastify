# Working with Docker Compose

## Basic commands

Start all services

```
docker-compose up
```

Run in the background

```
docker-compose up -d
```

To test it out, from another console window, issue a test curl command:

```
curl http://localhost:5000/ping
```

Stop all services

```
docker-compose down
```

Check the version of Docker Compose

```
docker-compose -v
```

## References

[Commands](https://docs.docker.com/engine/reference/commandline/compose/)
