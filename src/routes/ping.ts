import { FastifyInstance } from 'fastify';
import { StrictResource } from 'fastify-autoroutes';
import { SWAGGER_TAGS } from 'src/infrastructure/utils/swagger-tags';

export default (_fastify: FastifyInstance) =>
  <StrictResource>{
    get: {
      schema: {
        tags: [SWAGGER_TAGS.basic],
      },
      handler: (request, reply) => {
        reply.send('pong');
      },
    },
  };
