import { TagObject } from 'openapi3-ts';
import { Tag } from 'swagger-schema-official';

export const SWAGGER_TAGS = {
  basic: 'Basic',
};

/**
 * Build tags
 *
 * @return {{name: string, description: string}[]}
 */
export function buildTags() {
  const tags: Array<TagObject> | Array<Tag> = [];

  Object.values(SWAGGER_TAGS).forEach((name) => {
    tags.push({
      name,
      description: `${name} related endpoints`,
    });
  });

  return tags;
}
