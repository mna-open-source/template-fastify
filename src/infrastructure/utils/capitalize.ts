export const capitalize = (s: string) => {
  s = s.toLowerCase().replace(/-/g, ' ');
  return s.charAt(0).toUpperCase() + s.slice(1);
};
