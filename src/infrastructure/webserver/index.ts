import { FastifyInstance } from 'fastify';
import buildServer from './server';
import k from '../config/constants';

const fastifyOptions = JSON.parse(k.fastifyOptionsString);

const server: FastifyInstance = buildServer(fastifyOptions);

const start = async () => {
  try {
    await server.listen(k.port, '0.0.0.0');
  } catch (err) {
    server.log.error(err);
    process.exit(1);
  }
};

// Run the server!
start();

export default server;
