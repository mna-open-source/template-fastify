import { FastifyInstance, FastifyPluginCallback, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import FastifyXray from 'fastify-xray';

const plugin: FastifyPluginCallback<FastifyPluginOptions> = fp(
  (fastify: FastifyInstance, _options: FastifyPluginOptions, next: (err?: Error) => void) => {
    fastify.register(FastifyXray, {
      defaultName: `mna-fastify-${process.env.NODE_ENV}`,
    });

    next();
  },
);

export default plugin;
