import { FastifyInstance, FastifyPluginCallback, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import FastifyCompress from 'fastify-compress';

const plugin: FastifyPluginCallback<FastifyPluginOptions> = fp(
  (fastify: FastifyInstance, _options: FastifyPluginOptions, next: (err?: Error) => void) => {
    fastify.register(FastifyCompress, {
      encodings: ['gzip', 'deflate'],
    });

    next();
  },
);

export default plugin;
