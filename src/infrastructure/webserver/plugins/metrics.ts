import { FastifyInstance, FastifyPluginCallback, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import FastifyMetrics from 'fastify-metrics';

const plugin: FastifyPluginCallback<FastifyPluginOptions> = fp(
  (fastify: FastifyInstance, _options: FastifyPluginOptions, next: (err?: Error) => void) => {
    fastify.register(FastifyMetrics, {
      endpoint: '/metrics',
    });

    next();
  },
);

export default plugin;
