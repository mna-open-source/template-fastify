import { FastifyInstance, FastifyPluginCallback, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import FastifyOas from 'fastify-oas';
import k from '../../config/constants';
import { buildTags } from '../../utils/swagger-tags';

const plugin: FastifyPluginCallback<FastifyPluginOptions> = fp(
  (fastify: FastifyInstance, _options: FastifyPluginOptions, next: (err?: Error) => void) => {
    fastify.register(FastifyOas, {
      routePrefix: '/api/docs',
      swagger: {
        info: {
          title: k.packageName,
          description: k.packageDescription,
          version: k.packageVersion,
        },
        externalDocs: {
          url: 'https://swagger.io',
          description: 'Find more info here',
        },
        host: k.serverUrl.replace(/^https?:\/\//, ''),
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json'],
        tags: buildTags(),
        components: {
          securitySchemes: {
            bearerAuth: {
              type: 'http',
              scheme: 'bearer',
              bearerFormat: 'JWT',
            },
          },
        },
      },
      exposeRoute: true,
    });

    next();
  },
);

export default plugin;
