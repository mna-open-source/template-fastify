import { FastifyInstance, FastifyPluginCallback, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import FastifyHelmet from 'fastify-helmet';

const plugin: FastifyPluginCallback<FastifyPluginOptions> = fp(
  (fastify: FastifyInstance, _options: FastifyPluginOptions, next: (err?: Error) => void) => {
    //  Helmet to remove insecure headers
    fastify.register(FastifyHelmet);

    next();
  },
);

export default plugin;
