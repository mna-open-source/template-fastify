import Fastify, { FastifyInstance, FastifyPluginOptions } from 'fastify';
import FastifyAutoload from 'fastify-autoload';
import FastifyAutoRoutes from 'fastify-autoroutes';
import { StatusCodes } from 'http-status-codes';
import path from 'path';
import setErrorHandler from './errorHandler';
import k from '../config/constants';

export default function buildServer(options: FastifyPluginOptions): FastifyInstance {
  const server: FastifyInstance = Fastify(options);

  server.register(FastifyAutoload, {
    dir: path.join(__dirname, 'plugins'),
    options: { ...options },
  });

  server.register(FastifyAutoRoutes, {
    dir: k.routesDir,
    prefix: '/api',
  });

  setErrorHandler(server);

  server.get('/favicon.ico', (_request, reply) => {
    reply.code(StatusCodes.NO_CONTENT).send();
  });

  return server;
}
