import { FastifyInstance } from 'fastify';
import { StatusCodes } from 'http-status-codes';

export default function setErrorHandler(fastify: FastifyInstance): void {
  fastify.setErrorHandler((error: any, request, reply) => {
    if (error && error.validation) {
      request.log.info(`Context: ${error.validationContext}, errors: ${error.validation}`);

      reply.code(StatusCodes.UNPROCESSABLE_ENTITY).send({
        message: 'The fields were not submitted correctly',
        context: error.validationContext,
        error: error.validation,
      });
    } else {
      request.log.error(error);

      reply.code(StatusCodes.INTERNAL_SERVER_ERROR).send({
        message: 'Server error',
      });
    }
  });
}
