import * as path from 'path';
import * as os from 'os';
import isDocker from 'is-docker';

const hostname = os.hostname();

let address: string;

// to make it work (be exposed) when deployed in a container (Docker, etc) we need to listen not only to localhost but for example to all interfaces ...
if (!process.env.HTTP_ADDRESS) {
  address = isDocker() ? '0.0.0.0' : '127.0.0.1';
} else {
  address = process.env.HTTP_ADDRESS;
}

const protocol = 'http';
const port = process.env.HTTP_PORT ?? 80;
const rootDir = path.normalize(path.join(__dirname, path.sep, '..', path.sep, '..', path.sep, '..', path.sep));
const srcDir = path.normalize(path.join(rootDir, 'src', path.sep));
const routesDir = path.normalize(path.join(srcDir, 'routes', path.sep));

export default {
  packageName: process.env.npm_package_name || '',
  packageVersion: process.env.npm_package_version || '',
  packageDescription: process.env.npm_package_description || '',
  rootDir,
  srcDir,
  routesDir,
  fastifyOptionsString: process.env.FASTIFY_OPTIONS || '{ "logger": true, "ignoreTrailingSlash": true }',
  hostname,
  protocol,
  address,
  port,
  serverUrl: `${protocol}://${address}:${port}`,
};
