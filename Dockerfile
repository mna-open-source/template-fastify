FROM node:14-alpine AS builder

# couchbase sdk requirements
RUN apk update && apk add yarn curl bash git python g++ make && rm -rf /var/cache/apk/*

# install node-prune (https://github.com/tj/node-prune)
RUN curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin

WORKDIR /usr/src/app

COPY package.json ./

# install dependencies
RUN yarn --frozen-lockfile

COPY . .

# lint & test
# RUN yarn lint & yarn test

# build application
RUN yarn build

# remove development dependencies
RUN npm prune --production

# run node prune
RUN /usr/local/bin/node-prune

FROM alpine:3 AS server

LABEL version="1.0.0"
LABEL description="Fastify"
LABEL maintainer="Matias Nahuel Améndola <soporte.esolutions@gmail.com>"

RUN apk add nodejs=14.17.4-r0 --no-cache

WORKDIR /usr/src/app

# copy from build image
COPY --from=builder /usr/src/app/dist ./
COPY --from=builder /usr/src/app/node_modules ./node_modules

EXPOSE 80

HEALTHCHECK --interval=30s --timeout=10s --start-period=5s CMD NODE_PATH=./ node ./src/infrastructure/webserver/index.js

CMD NODE_PATH=./ node ./src/infrastructure/webserver/index.js
